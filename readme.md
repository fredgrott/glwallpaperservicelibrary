---

## GLWallpaperServiceLibrary

Its an Android Project Library for developing LiveWallpapers licensed under Apache 2.0 License.

## Credits

Robert Green

[his site] (http://rbgrn.net)

Mark F Guerra

[his github repos](http://github.com/markfguerra)
